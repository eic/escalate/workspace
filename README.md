[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/eic/escalate/workspace)

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/eic%2Fescalate%2Fworkspace/master)

# Escalate framework

> ESC - stands for EIC Software and Computing group. ESCalate framework uses ESC as the first letter of its name. 

A lightweight modular fast and full simulation framework for EIC. 
https://gitlab.com/eic/escalate

Escalate brings together the next software:

1. [G4E](https://gitlab.com/eic/escalate/g4e.git) - a lightweight pure Geant4 full simulation of EIC detectors and beamlines
2. [eJana](https://gitlab.com/eic/escalate/ejana.git) - multi-threaded modular High Energy and Nuclear Physics event reconstruction framework, that provides EIC related abstractions and leverages reconstruction and analysis tools such as ACTS and Genfit for tracking, Cern ROOT, HepMC and others.


To know where the software is located, call **ejpm** from terminal. 

![escalate_structure](./img/escalate_structure.png)


The framework provides modularity in both ways: 
- subpackage modularity: each package inside the framework can be used solely and separately from the rest of the framework
- subprocess modularity: jana (and ejana) modularize code in small libraries called plugins allowing to run tasks in a seamless multithreaded way. 


The Escalate itself ensures that data between packages is consistent and output of one package can be correctly read from another. And provides:
python orchestration to make it easier to configure each package and make it easy to organize a workflow between packages. Tools to install, maintain and deploy the subpackages


## Running in docker
*(extended information about options of running this docker containers)*

Running docker (JupyterLab):

```bash
docker run -it --rm -p8888:8888 electronioncollider/escalate
```

You can bind any directory on your system to docker image by using **-v** flag:

```
-v <your/directory>:<docker/directory>
```

Convenient place inside docker image is

```
/home/eicuser/workspace/share
```


**(!) Important to know (!)** Each time `docker run` command is called, it spawns a new container. 
In particular, it first creates a writeable container layer over the 
specified image, and then starts it using the specified command. A stopped container can be 
restarted with all its previous changes intact using `docker start`

Please, read extended docker instructions inside [Docker.md](Docker.md)


### Changing and saving the contents

The files that you see in Jupyter Lab are stored in Git repository: 
 
https://gitlab.com/eic/epw

So when you do the change, that is important to save, you could make 
you branch or fork the repo. This  


### Troubleshooting
If docker gives an error like this:
> Error starting userland proxy: listen tcp 0.0.0.0:8888: bind: address already in use.

It usually means, that the port 8888 is used by another application. 
To fix that try to change `-p 8888:8888` flag to `-p <something>:8888` 
e.g. `-p 9999:8888`. Put the same port in your browser:
```
127.0.0.1:9999/lab
```
