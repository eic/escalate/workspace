# EIC docker images
The primary goal of the images is to provide an easy means for scientists to start running EIC software. There 4 main images:


* electronioncollider/eic-science
* electronioncollider/eic-mceg
* electronioncollider/escalate
* electronioncollider/escalate-gui (in development(!))

![Hierarhy](img/docker-hierarhy.svg)

[EIC software quick start tutorial](https://eic.gitlab.io/documents/quickstart/)


## Running in docker
*(extended information about options of running this docker containers)*

To update the container

```bash
docker pull electronioncollider/escalate
```

Running docker (JupyterLab):

```bash
docker run -it --rm -p8888:8888 electronioncollider/escalate
```


**(!) Important to know (!)** Each time `docker run` command is called, it spawns a new container. 
In particular, it first creates a writeable container layer over the 
specified image, and then starts it using the specified command. A stopped container can be 
restarted with all its previous changes intact using `docker start`


Docker works somehow like tmux or screen - you can reconnect to the running image, 
attach many bash shells and even if container is stopped you can reconnect.
This makes debugging easier and you retain all your data. Unless you use `--rm` flag...


```--rm``` flag Docker **automatically cleans up the container** and remove the file system 
**when the container exits**. 

Docker documentation: 
[docker run](https://docs.docker.com/engine/reference/commandline/run/), 
[docker start](https://docs.docker.com/engine/reference/commandline/start/), 
[--rm flag](https://docs.docker.com/engine/reference/run/#clean-up---rm). 

We use --rm flag for the sake of the tutorial repeatability. **If you work with the container, 
might be, you don't need it**. 


```-it``` flag enables interactive session. Without this flag ctrl+c will not work on Mac 
machines. In general `-it` is used to run e.g. bash session (see below)

**Ports:**  `8888` - for Jupyter Lab, `6080` - for noVNC viewer (escalate-gui image only), `5901` - to access with any VNC (escalate-gui image only)


## Add your system directory inside docker

You can bind any directory on your system to docker image by using **-v** flag:

```
-v <your/directory>:<docker/directory>
```

Convenient place inside docker image is

```
/home/eicuser/workspace/share
```

More information on [docker bind](https://docs.docker.com/storage/bind-mounts/)

There are also other mechanisms of how to manage data in docker. Please read [the official documentation on managing data in docker](https://docs.docker.com/storage/)



**Debugging** : To do C++ debugging (run GDB or so) one has to specify additional flags 
(the debugging is switched off by docker by default for security reasons):

```--cap-add=SYS_PTRACE --security-opt seccomp=unconfined```



### Starting options:

With debugging enabled and all ports open: 

```
docker run -it --rm --cap-add=SYS_PTRACE --security-opt seccomp=unconfined -p8888:8888 electronioncollider/escalate
```

JupyterLab only:

```
# Jupyter only:
docker run -it --rm -p8888:8888 electronioncollider/escalate
```

You can start the docker without JupyterLab/vnc running by adding `bash` in the end

```bash
docker run --rm -it -p 8888:8888 electronioncollider/epic:latest bash
```

Later you can still run jupyter lab by 

```bash
jlab    # It is Jupyter Lab here, not Jefferson
```


## Explore more

- [Jupyter community guides](https://jupyter.readthedocs.io/en/latest/community/content-community.html)
- [Python Data Science Handbook](https://github.com/jakevdp/PythonDataScienceHandbook)



# X11 - Working with GUI

There are several ways of dealing with native GUI applications for `escalate` and `escalate-gui` images. E.g. showing standard root browser or Geant4 event viewer. 


1. SSH -X
2. X11 directly
3. noVNC(in browser)
4. Any other VNC viewer
5. Remote tools

What is the best option:


## 1. SSH -X
eicuser password is eicuser

```
docker run --rm -it -p127.0.0.1:2222:22 -p8888:8888 electronioncollider/escalate:latest runssh
```

connect with SSH
```
ssh -X eicuser@127.0.0.1 -p 2222
```


## 2. X11

The most convenient is using X11 directly. It require x11 client apps on Macs and Windows and may have some issues with user id's and permissions on Posix (max & linux). 
It might sound complex, but actiually it is simple and works most of the times. Still we don't use this way for the tutorials, but it is available in the documentation. 

**Requirements**: X11 cliens (windows and mac), additional docker flags (see of each OS)


You can use X11 natively (as natively as possible) with this docker image in your system:

### Linux

To use graphics, make sure you are in an X11 session and run the following command: 

```bash
docker run -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix --rm -it --user $(id -u) -p8888:8888 electronioncollider/escalate
```

There might be issues with user id on systems like JLab farms. 

### Windows

To enable graphics, you must have [VcXsrv](https://sourceforge.net/projects/vcxsrv/files/latest/download) installed. 
Make sure VcXsrv is whitelisted in the Windows firewall when prompted. 

Start VcXsrv with 'allow from any origin' flag

```bash
docker run --rm -it -p 8888:8888 -e LIBGL_ALWAIS_INDIRECT=1 -e DISPLAY=10.0.75.1:0  electronioncollider/escalate bash
```

> 10.0.75.1 address corresponds to the network configuration in docker settings


### OSX

To use graphics on OSX, make sure XQuarz is installed. 
After installing, open XQuartz, and go to XQuartz, Preferences, select the Security tab, and tick the box 
"Allow connections from network clients". Then exit XQuarz. 

Afterwards, open a terminal and run the following commands: 

```bash
ip=$(ifconfig en0 | grep inet | awk '$1=="inet" {print $2}') 

echo $ip   # To make sure it was successfull
           # If nothing is displayed, replace en0 with en1 and so on
           
xhost + $ip  # start XQuartz and whitelist your local IP address

``` 

This will start XQuartz and whitelist your local IP address. 

Finally, you can start up docker with the following command: 

```
docker run --rm -it -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=$ip:0 -p8888:8888 electronioncollider/escalate
```


## 3. noVNC

(!) will be available in **escalate-gui** image which is in the development

**Procs**: Works out of the box in a browser and doesn't require any additional software on user's machine. So it is very reliable for tutorials or on new/unknown machines. 

**Cons**: is slow and is good as a solution which

**Requirements**: add `-p6080:6080` to docker run string



## 4. VNC viewer

(!) will be available in **escalate-gui** image which is in the development

One can use any VNC viewer with the docker image. 

**Requirements**: add `-p5901:5901` to docker run string

## 4. Remote debugging

While not directly connected with running native GUI in docker, it might be a very convenient workflow of working with the docker. 

It is possible to use development tools (editors, IDE's, debuggers, etc.) installed on your system, using the docker to build, run and debug the code.

The setup is depends on particular tools and usually falls to "remote debugging" section of documentations, tutoriels, etc.


Here is the instrucions about the remote debugging of it:  
https://code.visualstudio.com/docs/remote/remote-overview


**Credits**:

The EIC Container project is coordinated by [David Lawrence](mailto:davidl@jlab.org) and [Dmitry Romanov](mailto:romanov@jlab.org).
