
## Standalone plugins
We started with eJANA as one repo with plugins inside. This setup is very common in JLab JANA projects, when there is a collaboration, people working on different plugins/code parts in one repository with branches, forks, etc. But YP studies showed us, that this is wrong approach for YP because having just one repo with eJANA is very inconvenient. So there we see 3 issues, that we are fixing to make workflow convenient:
Plugins with user analysis have to be standalone, users put them in their own repositories. eJANA provide plugins like opening files, smearing and track reconstruction.

There should be a way to generate plugin stubs.

It would be good if plugins get compiled on fly when jana starts.


So in the end I see the workflow like this: you can generate a plugin with EIC analysis stub with one line and it will be automatically checked and compiled on Jana start (or you can compile and debug it yourself, it is just a library in the end).
Users work with plugins in their repositories and it would be cool (in future) if user can just provide repo link to eJANA to run the plugin. This would also play nicely with sending jobs to batch processing. (edited) 