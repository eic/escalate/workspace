#ifndef ffe_LUMI_HH
#define ffe_LUMI_HH

#include <spdlog/spdlog.h>

#include <InitializationContext.hh>


struct ffe_LUMI_Config
{
    // define here Global volume parameters
    double SizeX = 2. * m;        // <<<<<<<<<<<<<<
    double SizeY = 2. * m;        // <<<<<<<<<<<<<<
    double SizeZ = 30 * m;        // <<<<<<<<<<<<<<
    double PosX = 2 * m;          // <<<<<<<<<<<<<<
    double PosY = 0 * m;          // <<<<<<<<<<<<<<
    double PosZ = -30 * m;        // <<<<<<<<<<<<<<
};


class ffe_LUMI_Design
{
public:

    /// Constructs outer or 'mother' volume for the detector
    inline void Construct(ffe_LUMI_Config cfg, G4Material *worldMaterial, G4VPhysicalVolume *motherVolume)
    {
        ConstructionConfig = cfg;

        spdlog::debug("Initialize ffe_LUMI volume");

        // create  a global volume for your detectors here:
        Solid = new G4Box("ffe_LUMI_GVol_Solid", cfg.SizeX / 2., cfg.SizeY / 2., cfg.SizeZ / 2.);   // <<<<<<<<<<<<<<
        Logic = new G4LogicalVolume(Solid, worldMaterial, "ffe_LUMI_GVol_Logic");                   // <<<<<<<<<<<<<<
        Phys = new G4PVPlacement(nullptr, G4ThreeVector(cfg.PosX, cfg.PosY, cfg.PosZ),              // <<<<<<<<<<<<<<
                "ffe_LUMI_GVol_Phys", Logic, motherVolume, false, 0);                               // <<<<<<<<<<<<<<

        // Visual attributes
        VisAttr = new G4VisAttributes(G4Color(0.3, 0, 3., 0.1));
        VisAttr->SetLineWidth(1);
        VisAttr->SetForceSolid(true);
        Logic->SetVisAttributes(VisAttr);                                                           // <<<<<<<<<<<<<<
        // Logic->SetVisAttributes(G4VisAttributes::GetInvisible());
    };


    /// Construct internal parts of the detector
    inline void ConstructDetectors()
    {
        spdlog::debug("Begin ffe_LUMI internal detector construction");

        // construct here your detectors
    };


    // Public variables
    G4Box *Solid;              // pointer to the solid (!) Might be not G4Tubs
    G4LogicalVolume *Logic;     // pointer to the logical
    G4VPhysicalVolume *Phys;    // pointer to the physical
    G4VisAttributes *VisAttr;   // Visual attributes

    /// Parameters that was used in the moment of construction
    ffe_LUMI_Config ConstructionConfig;

    // constructor
    // ffe_LUMI_Design(g4e::InitializationContext& context):fContext(context){}
private:

    // if initialization context is needed
    // g4e::InitializationContext &fContext;

    // define here local variables and parameter of detectors
};


#endif // ffe_LUMI_HH
