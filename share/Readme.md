### Sharing files 
A directory on the local host can be bound to the EIC Software image to share files between the local system and the Docker process. 
```sh
docker run -it --rm -p 8888:8888 -v /specific/directory/:/home/eicuser/epw/share eicdev/epic
```
In the example command, _/specific/directory_ refers to a directory on the local host, e.g., _~/Desktop/share_ or _/home/unknown/eic/_, and _/home/eicuser/epw/share_ to the shared directory in the EIC Software image. The files in _/specific/directory_ will shown in the JuypterLab environment in the folder _share_ (and vice versa). 
