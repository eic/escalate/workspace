# This dockerifle is mainly for Binder

FROM electronioncollider/escalate@sha256:2e6201fb5906ffb1156f7c998090460012fc40e133b7a94faee88ab01fafb672

USER eicuser
WORKDIR /home/eicuser

RUN mv workspace/* ./ && rm -rf workspace